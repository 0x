# db-db = dingbat database
use strict;

my %dict = (
0x5c => "backslash",
0x488 => "combining cyrillic hundred thousands sign",
0xe5b => "thai character khomut",
0x229a => "circled ring operator",
0x2399 => "print screen symbol",
0x23cf => "eject symbol",
0x2581 => "lower one eight block",
0x2583 => "lower three eights block",
0x2585 => "lower five eights block",
0x25b3 => "white up-pointing triangle",
0x25b8 => "black right-pointing small triangle",
0x2614 => "umbrella with rain drops",
0x263a => "white smiling face",
0x2665 => "black heart suit",
0x26a1 => "high voltage sign",
0x2708 => "airplane",
0x270c => "victory hand",
0x2739 => "twelve pointed black star",
0x274d => "shadowed white circle",
0x27ab => "black tilted shadowed white rightwards arrow",
0xe000 => "OSP_Frog (private)",
0xe002 => "Cimatics_scare_eye (private)",
0xe003 => "white_pentagon (private)",
0xe004 => "cimatics glissando has moved (private)",
0xe005 => "cimatics_palm (private)"
);
my %tcid = reverse %dict;;

open NAMES, ">:utf8", "names.txt";
open POINTERS, ">:utf8", "pointers.txt";
open CHARS, ">:utf8", "characters.txt";
my $key;
my $value;
# perl cookbook recipe 4.15
foreach $key (sort {$a <=> $b } keys %dict) {
    $value = $dict{$key};
    print NAMES $value, "\n";
    printf POINTERS "%#x\n", $key;
    print CHARS chr($key), "\n";
}

close NAMES;
close POINTERS;
close CHARS;
