#!/usr/bin/env perl

# This software is part of the OSP dingbat suite.

use strict;
use File::Find;

# unicode support
use utf8;
use open ':encoding(utf8)';
binmode(STDOUT, ":utf8");

# yes, there's Unicode::UCD. this script is monadic...
my %symbols = (
	currency_symbols => [0x20A0..0x20CF],
	letterlike_symbols => [0x2100..0x214F],
	number_forms => [0x2150..0x218F],
	mathematical_operators => [0x2200..0x22FF],
	supplemental_mathematical_operators => [0x2A00..0x2AFF],
	miscellaneous_mathematical_symbols_a => [0x27C0..0x27EF],
	miscellaneous_mathematical_symbols_b => [0x2980..0x29FF],
	miscellaneous_symbols_and_arrows => [0x2B00..0x2BFF],
	arrows => [0x2190..0x21FF],
	control_pictures => [0x2400..0x243F],
	miscellaneous_technical => [0x2300..0x23FF],
	optical_character_recognition => [0x2440..0x245F],
	geometric_shapes => [0x25A0..0x25FF],
	miscellaneous_symbols => [0x2600..0x26FF],
	dingbats => [0x2700..0x27BF],
	yijing_hexagram_symbols => [0x4DC0..0x4DFF],
	enclosed_alphanumerics => [0x2460..0x24FF],
	enclosed_cjk_letters_and_months => [0x3200..0x32FF],
	cjk_compatibility => [0x3300..0x33FF],
	braille_patterns => [0x2800..0x28FF],
);

my @blocks = keys %symbols;
%symbols = &filter_empty(\%symbols);
my %secret_code = ();

find(\&file2secret, @ARGV);

sub filter_empty {
	# this procedure filters out unassigned unicode code points
	my %symbols = %{$_[0]};
	while ( my ($key, $value) = each %symbols) {
		my @remove_empty = ();
		foreach (@{$value}) {
			if (chr($_) =~ /\p{Assigned}/) {
				push @remove_empty, $_;
			}
		}
		$symbols{$key} = \@remove_empty;
	}
	%symbols;
}

sub file2secret {
	my $current_file = $_;
	open FILE, "<$current_file";
	open TEMP, ">temp";
	while (<FILE>) {
		my $line = $_;
		foreach my $char (split //, $line) {
			if ($char =~ /\s/) {
				print TEMP $char;
			}
			elsif ($secret_code{$char}) {
				print TEMP $secret_code{$char};
			}
			else {
				my @random_block = @{$symbols{$blocks[int(rand(@blocks))]}};
				$secret_code{$char} = chr($random_block[int(rand(@random_block))]);
				print TEMP $secret_code{$char};
			}
		}
	}
	close FILE;
	close TEMP;
	unless ($current_file eq "temp" || $current_file eq ".") {
		rename("temp", &filename2symbol($current_file));
	}
	unlink $current_file;
}

sub filename2symbol {
	# this procedure is used to rename files with symbols
	my $new_name = "";
	foreach my $char (split //, $_[0]) {
		if ($secret_code{$char}) {
			$new_name .= $secret_code{$char};
		}
		else {
			my @random_block = @{$symbols{$blocks[int(rand(@blocks))]}};
			$secret_code{$char} = chr($random_block[int(rand(@random_block))]);
			$new_name .= $secret_code{$char};
		}
	}
	$new_name;
}

# in one issue he used Dingbat as the font for what he considered a rather
# dull interview
