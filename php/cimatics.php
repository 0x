<html>
	<head>
<style>
#container {
	font-size : 500%;
	line-height : 1.125em;
	padding-top : 2%;
	padding-left : 2%;
	font-family : cimatics;
}

#container .tux {
	font-family : libertine;
}

@font-face {
	font-family : libertine;
	src : url("LinLibertine_Re-4.4.1.otf");
}

@font-face {
	font-family : dlf;
	src : url("Cimatics-Base.ttf");
}
</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	</head>
	<body>

<div id="container">

<?php

$dlf = array(0x26, 0x2602, 0x2606, 0x2609, 0x260a, 0x2612, 0x2618, 0x2619, 0x2629, 0x262a, 0x262e, 0x2633, 0x2634, 0x263c, 0x263e, 0x2647, 0x264d, 0x265e, 0x265f, 0x2667, 0x2668, 0x266a, 0x2670, 0x2676, 0x2682, 0x2689, 0x268b, 0x268f, 0x2691, 0x2695, 0x2696, 0x269e, 0x26a6, 0x26ab, 0x26ac, 0x26b0, 0x26b6, 0x26bd, 0x26be, 0x26c1, 0x26cf, 0x26d8, 0x26d9, 0x26da, 0x26e1, 0x26ef, 0x26f0, 0x26f6, 0x26fa, 0x26fe, 0x2706, 0x2709, 0x270f, 0x2710, 0x2716, 0x271b, 0x2723, 0x2726, 0x2736, 0x2738, 0x273c, 0x273d, 0x2742, 0x2743, 0x2752, 0x2756, 0x275b, 0x275d, 0x275e, 0x2762, 0x276a, 0x276c, 0x2770, 0x2771, 0x278e, 0x2790, 0x2796, 0x2799, 0x279a, 0x27ac, 0x27af, 0x27b5, 0x27b7, 0x27bc);

$cimatics = array(0x5c, 0x488, 0xe5b, 0x229a, 0x2399, 0x23cf, 0x2581, 0x2583, 0x2585, 0x25b3, 0x25b8, 0x2614, 0x263a, 0x2665, 0x26a1, 0x2708, 0x270c, 0x2739, 0x274d, 0x27ab, 0xe000, 0xe002, 0xe003, 0xe004, 0xe005);

/**
 * Writes text blocks of rows*columns size with characters from an array of
 * codepoints. Some of the characters written are randomly-distributed spaces.
 * @param array $codepoints an array of codepoints
 * @param integer $columns number of characters per line
 * @param integer $rows number of lines
 */
function around_text_symbols($codepoints, $columns, $rows) {
	for ($row = 1; $row <= $rows; $row++) {
		$random_distributed_space = range(1, $columns);
		shuffle($random_distributed_space);
		array_splice($random_distributed_space, mt_rand(1, $columns));
		for ($column = 1; $column <= $columns; $column++) {
			if (in_array($column, $random_distributed_space)) {
				print "&nbsp;";
			} else {
				$my_choice = array_rand($codepoints, 2);
				if ($codepoints[$my_choice[0]] == 57344) {
					$tux = mt_rand(0, 1);
					if ($tux == 1) {
						print "<span class=\"tux\">&#x" . dechex($codepoints[$my_choice[0]]) . ";</span>";
					} else {
						print "&#x" . dechex($codepoints[$my_choice[0]]) . ";";
					}
				} else {
					print "&#x" . dechex($codepoints[$my_choice[0]]) . ";";
				}
			}
		}
		print "<br/>";
	}
}

around_text_symbols($cimatics, 10, 8);

?>


</div>

	</body>
</html>
