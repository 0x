<style>
	table {
		display: inline-table;
		margin: 20px;
		font-size: xx-large;
		border-collapse: collapse;
	}
	td {
		border: 1px solid;
		padding: 5px;
	}
</style>
<?php
# take a hexadecimal number and return an html char entity!
function pointer2html($pointer) {
	return "&#x" . $pointer . ";";
}
# the size of the `less' array is less than or equal to the size of the `more' 
# array
function one_perm($less, $more) {
    shuffle($more);
    $thismuch_less = 0;
    foreach ($less as $a_less) {
		printf("<tr><td>%s</td><td>%s</td></tr>", pointer2html($a_less), pointer2html($more[$thismuch_less]), $more[$thismuch_less]);
        $thismuch_less++;
    }
};

$notpunctuation = array("5c",
    "488",
    "e5b",
    "229a",
    "2399",
    "23cf",
    "2581",
    "2583",
    "2585",
    "25b3",
    "25b8",
    "2614",
    "263a",
    "2665",
    "26a1",
    "2708",
    "270c",
    "2739",
    "274d",
    "27ab");
$punctuation = array("21",
    "22",
    "27",
    "28",
    "29",
    "2c",
    "2e",
    "3a",
    "3f");
for ($i = 1; $i <= 20; $i++) {
    print "<table>";
    one_perm($punctuation, $notpunctuation);
    print "</table>";
}
?>
